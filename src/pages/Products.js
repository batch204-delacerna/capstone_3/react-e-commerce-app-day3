import { useEffect, useState, useContext } from 'react';
import ProductCard from '../components/ProductCard';
import AdminView from '../components/AdminView';
import UserContext from '../UserContext';

export default function Products(){

	const [productsData, setProductsData] = useState([]);

	const { user } = useContext(UserContext)

	// Props
		// is a shorthand for "property" since components are considered as object in ReactJS.
		// Props is a way to pass data from a parent component to a child component.
		// It is synonymous to the function parameter.

	//Fetch by default always makes a GET request, unless a different one is specified
	//ALWAYS add fetch requests for getting data in a useEffect hook

	const fetchData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products`)
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			setProductsData(data)
		})
	}

	useEffect(() => {
		// console.log(process.env.REACT_APP_API_URL)
		// changes to env files are applied ONLY at build time (when starting the project locally)

		fetchData()
	}, [])

	const products = productsData.map(product =>{
		if(product.isActive){
			return(
				<ProductCard productProp={product} key={product._id} />
			)
		}else{
			return null
		}
	})

	return(
		(user.isAdmin) ?
		<AdminView productProp={productsData} fetchData={fetchData}/>
		:
		<>
			<h1>Products</h1>
			{products}
		</>
	)
}
