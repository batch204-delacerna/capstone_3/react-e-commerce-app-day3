import { useContext } from 'react';
import { Container, Nav, Navbar } from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavBar() {

	// A context object such as our UserContext can be "opened" with React's useContext hook
	const { user } = useContext(UserContext);

	return(
		<Navbar bg="secondary" expand="lg">
		      <Container>
		        <Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		        {/*
		        	mr -> me
					ml -> ms
		        */}
		          <Nav className="ms-auto">
		            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
		            <Nav.Link as={NavLink} to="/products">Products</Nav.Link>

		            {(!user.id) ?
		            	//(user.id !== null && user.id !== undefined) ?
		            	<>
		            		<Link className="nav-link" to="/login">Login</Link>
		            		<Link className="nav-link" to="/register">Register</Link>
		            	</>
		            	:
		            	<Link className="nav-link" to="/logout">Logout</Link>
		            }
		          </Nav>
		        </Navbar.Collapse>
		      </Container>
		    </Navbar>
	);
}